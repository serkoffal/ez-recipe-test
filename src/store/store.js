import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);

export default () => {
  return new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
      favorites: []
    },
    getters: {
      FAVORITES: state => {
        return state.favorites;
      }
    },
    mutations: {
      changeFavorites: function(state, payload) {
        let favoriteIndex = state.favorites.indexOf(payload.name);
        favoriteIndex !== -1 ? state.favorites.splice(favoriteIndex, 1) : state.favorites.push(payload.name);
      }
    }
  })
}